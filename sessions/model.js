const mongoose = require('mongoose');
const Schema = mongoose.Schema;

mongoose.Promise = global.Promise;

const SessionSchema = Schema({
  title: { type: String, required: true, default: "new session" },
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  playlists: [{ type: Schema.Types.ObjectId, ref: 'Playlist' }]
});

const Session = mongoose.model('Session', SessionSchema);

module.exports = { Session };