const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const { Session } = require('./model');
const { Playlist } = require("../playlists/model");
const { Track } = require('../tracks/model');

const router = express.Router();

router.use(bodyParser.json());

// get ALL sessions - probably won't need this for production
router.get('/', (req, res) => {
  console.log('getting all sessions');
  Session.find((err, sessions) => {
    if (err) res.send(err);
    res.json(sessions);
  });
});

// get a specific session - provide session ID in request URL
router.get('/:id', (req, res) => {  
  Session
    .findById(req.params.id)
    .populate('user')
    .populate({
      path: 'playlists',
      populate: { path: 'tracks' }
    })
    .exec((err, session) => {
      if (err) res.json(err);
      res.json(session)
    });
});

// get all playlists for specific session - provide session ID in request URL
router.get('/:id/playlists', (req, res) => {
  Playlist
    .find({ session: req.params.id })
    .populate('tracks')
    .exec((err, playlists) => {
      if (err) res.json(err);
      res.json(playlists);
    });
});

// add a new playlist to a session - provide session ID in request URL
// and playlist title in request body
router.post('/:id/playlists', (req, res) => {
  const newPlaylist = new Playlist({
    title: req.body.title,
    session: req.params.id
  });

  Session
    .findById(req.params.id)
    .then(session => {
      session.playlists.push(newPlaylist);
      session.save();
    })
    .then(newPlaylist.save())
    .then(res.json(newPlaylist))
    .catch(err => res.json(err));
});

// delete a session (cascades down and deletes children/grandchildren) - provide session ID in request URL
router.delete('/:id', (req, res) => {
  Session
    .findByIdAndRemove(req.params.id)
    .then(session => {
      session.playlists.forEach((playlist, index) => {
        Playlist.findByIdAndRemove(playlist).exec()
          .then(playlist => {
            playlist.tracks.forEach((track, index) => {
              Track.findByIdAndRemove(track).exec();
            });
          });
      });
    })
    .then(session => res.json(session))
    .catch(err => res.json(err));
});


module.exports = { router };