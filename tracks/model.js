const mongoose = require('mongoose');
const Schema = mongoose.Schema;

mongoose.Promise = global.Promise;

const TrackSchema = mongoose.Schema({
  title: { type: String, required: true },
  url: { type: String, required: true },
  type: { type: String },
  description: { type: String },
  playlist: { type: Schema.Types.ObjectId, ref: "Playlist" }
});

const Track = mongoose.model('Track', TrackSchema);

module.exports = { Track };