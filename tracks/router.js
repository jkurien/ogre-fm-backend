const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const { Track } = require('./model');
const { Playlist } = require("../playlists/model");

const router = express.Router();

router.use(bodyParser.json());

// get ALL tracks in the db --probably won't need this for production
router.get('/all', (req, res) => {
  Track.find((err, tracks) => {
    if (err) res.send(err);
    res.json(tracks);
  });
});

// get a specific track - requires track ID in request URL
router.get('/:id', (req, res) => {
  Track
    .findById(req.params.id)
    .populate('playlist')
    .exec((err, track) => {
      if (err) res.json(err);
      res.json(track);
    });
});

// delete a track - provide track ID in request URL
router.delete('/:id', (req, res) => {
  Track
    .findByIdAndRemove(req.params.id)
    .then(track => res.json(track))
    .catch(err => res.json(err));
});

module.exports = { router };