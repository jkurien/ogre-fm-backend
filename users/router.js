const express = require('express');
const bodyParser = require('body-parser');
const { User } = require("./model");
const { Session } = require('../sessions/model');
const { Playlist } = require('../playlists/model');
const { Track } = require('../tracks/model');

const router = express.Router();

router.use(bodyParser.json());

// get ALL users - probably won't need this for production
router.get('/', (req, res) => {
  User.find((err, users) => {
    if (err) res.json(err);
    res.json(users);
  });
});

// get a specific user - provide userID in request URL
router.get('/:id', (req, res) => {
  User
    .findById(req.params.id)
    .populate({
      path: 'sessions',
      populate: ({ 
        path: 'playlists',
        populate: { path: 'tracks' }
       })
    })
    .exec((err, user) => {
      if (err) res.json(err);
      res.json(user);
    });
});

// get all sessions for specific user - provide userID in request URL
router.get('/:id/sessions', (req, res) => {
  Session
    .find({ user: req.params.id })
    .populate({
      path: 'playlists',
      populate: { path: 'tracks' }
    })
    .exec((err, sessions) => {
      if (err) res.json(err);
      res.json(sessions);
    });
});

// add new user - provide username and password in request body -- NO ENCRYPTION YET
router.post('/', (req, res) => {
  const newUser = new User({
    username: req.body.username,
    password: req.body.password
  });

  newUser.save()
    .then(res.json(newUser))
    .catch(err => res.json(err));
});

// add a new session to a user - provide userID in request URL
// and session title in request body
router.post('/:id/sessions', (req, res) => {
  const newSession = new Session({
    title: req.body.title,
    user: req.params.id
  });

  User
    .findById(req.params.id)
    .then(user => {
      user.sessions.push(newSession);
      user.save();
    })
    .then(newSession.save())
    .then(res.json(newSession))
    .catch(err => res.json(err));
});

// delete a user (cascades down and deletes children/grandchildren)
// - provide userID in request URL
router.delete('/:id', (req, res) => {
  User
    .findByIdAndRemove(req.params.id)
    .then(user => {
      user.sessions.forEach((session, index) => {
        Session.findByIdAndRemove(session).exec()
        .then(session => {
          session.playlists.forEach((playlist, index) => {
            Playlist.findByIdAndRemove(playlist).exec()
            .then(playlist => {
              playlist.tracks.forEach((track, index) => {
                Track.findByIdAndRemove(track).exec();
              });
            });
          });
        });
      });
    })
    .then(user => res.json(user))
    .catch(err => res.json(err));
});

// DANGER ZONE - wipe db
router.delete('/', (req, res) => {
  User.find().remove().exec();
  res.send('all users deleted');
});

module.exports = { router };