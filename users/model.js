const mongoose = require('mongoose');
const Schema = mongoose.Schema;

mongoose.Promise = global.Promise;

const UserSchema = Schema({
  username: { type: String, required: true, default: 'newuser' },
  password: { type: String, required: true, default: 'password' },
  sessions: [{ type: Schema.Types.ObjectId, ref: 'Session' }]
});

const User = mongoose.model('User', UserSchema);

module.exports = { User };