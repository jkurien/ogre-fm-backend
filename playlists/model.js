const mongoose = require('mongoose');
const Schema = mongoose.Schema;

mongoose.Promise = global.Promise;

const PlaylistSchema = Schema({
  title: { type: String, required: true, default: "new playlist" },
  session: { type: Schema.Types.ObjectId, ref: "Session" },
  tracks: [{ type: Schema.Types.ObjectId, ref: 'Track' }]
});

const Playlist = mongoose.model('Playlist', PlaylistSchema);

module.exports = { Playlist };