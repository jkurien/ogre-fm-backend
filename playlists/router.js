const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const { Playlist } = require("./model");
const { Track } = require("../tracks/model");

const router = express.Router();

router.use(bodyParser.json());

// get ALL playlists in the db - probably won't need this for production
router.get('/', (req, res) => {
  Playlist.find((err, playlists) => {
    if (err) res.send(err);
    res.json(playlists);
  });
});

// get specific playlist - provide playlist ID in request URL
router.get('/:id', (req, res) => {
  Playlist
    .findById(req.params.id)
    .populate("session")
    .populate("tracks")
    .exec((err, playlist) => {
      if (err) res.json(err);
      res.json(playlist);
  });
});

// get all tracks for specific playlist - provide playlist ID in request URL
router.get('/:id/tracks', (req, res) => {
  Track
    .find({ playlist: req.params.id })
    .populate('playlist')
    .exec((err, tracks) => {
      if (err) res.json(err);
      res.json(tracks);
    });
});

// add a new track to a playlist - provide playlist ID in request URL
// and track title/track url in request body
router.post('/:id/tracks', (req, res) => {
  const newTrack = new Track({
    title: req.body.title,
    url: req.body.url,
    playlist: req.params.id
  });

  Playlist
    .findById(req.params.id)
    .then(playlist => {
      playlist.tracks.push(newTrack);
      playlist.save();
    })
    .then(newTrack.save())
    .then(res.json(newTrack))
    .catch(err => res.json(err));
});

// delete a playlist (cascades down and deletes children) - provide playlist ID in request URL
router.delete('/:id', (req, res) => {
  Playlist
    .findByIdAndRemove(req.params.id)
    .then(playlist => {
      playlist.tracks.forEach((track, index) => {
        Track.findByIdAndRemove(track).exec();
      });
    })
    .then(playlist => res.json(playlist))
    .catch(err => res.json(err));
});

module.exports = { router };