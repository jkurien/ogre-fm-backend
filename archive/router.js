const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const { Track } = require("../tracks/model");

const router = express.Router();

router.use(bodyParser.json());

// get all music tracks in the archive
router.get('/music', (req, res) => {
  Track
    .find({ type: 'music' })
    .then(result => res.json(result))
    .catch(err => res.json(err));
});

// get all ambience tracks in the archive
router.get('/ambience', (req, res) => {
  Track
    .find({ type: 'ambience' })
    .then(result => res.json(result))
    .catch(err => res.json(err));
});

// add a new track to the archive - provide info in request body
router.post('/', (req, res) => {
  const newTrack = new Track({
    title: req.body.title,
    url: req.body.url,
    type: req.body.type,
    description: req.body.description
  });

  newTrack.save()
    .then(res.json(newTrack))
    .catch(err => res.json(err));
});

module.exports = { router };