const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const { router: usersRouter } = require ('./users/router');
const { router: authRouter } = require('./auth/router');
const { router: sessionsRouter } = require('./sessions/router');
const { router: playlistsRouter } = require('./playlists/router');
const { router: tracksRouter } = require('./tracks/router');
const { router: archiveRouter } = require('./archive/router');

mongoose.Promise = global.Promise;

const app = express();

const { PORT, DATABASE_URL } = require('./config');

app.use(morgan('common'));

app.use('/api/users/', usersRouter);
app.use('/api/auth/', authRouter);
app.use('/api/sessions/', sessionsRouter);
app.use('/api/playlists/', playlistsRouter);
app.use('/api/tracks/', tracksRouter);
app.use('/api/archive/', archiveRouter);
mongoose.connect(DATABASE_URL);

app.get('/api/', (req, res) => {
  return res.send('hello from api');
});

let server;

function runServer() {
  return new Promise((resolve, reject) => {
    server = app.listen(PORT, () => {
      console.log(`Your app is listening on port ${PORT}`);
      resolve(server);
    }).on('error', err => {
      reject(err)
    });
  });
}

function closeServer() {
  return new Promise((resolve, reject) => {
    console.log('Closing server');
    server.close(err => {
      if (err) {
        reject(err);
        return;
      }
      resolve();
    });
  });
}

if (require.main === module) {
  runServer().catch(err => console.error(err));
};

module.exports = { app, runServer, closeServer };

